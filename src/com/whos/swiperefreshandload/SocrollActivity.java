package com.whos.swiperefreshandload;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.whos.swiperefreshandload.view.CircleProgress;
import com.whos.swiperefreshandload.view.SwipeProgressBar;
import com.whos.swiperefreshandload.view.SwipeProgressInterface;
import com.whos.swiperefreshandload.view.SwipeRefreshLayout;
import com.whos.swiperefreshandload.view.SwipeRefreshLayout.OnLoadListener;
import com.whos.swiperefreshandload.view.SwipeRefreshLayout.OnRefreshListener;


@SuppressLint("ResourceAsColor")
public class SocrollActivity extends Activity implements OnRefreshListener, OnLoadListener{

    protected ViewGroup mListView;
    private ArrayAdapter<String> mListAdapter;
    SwipeRefreshLayout mSwipeLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroller);

        mListView = (ViewGroup) findViewById(R.id.list);
        mListAdapter = new ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_1, values);
		setAdapter(mListView,(mListAdapter));


        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setOnLoadListener(this);
        SwipeProgressInterface topProgress = new SwipeProgressBar(mSwipeLayout);
        SwipeProgressInterface bottomProgress = new CircleProgress(mSwipeLayout);
        mSwipeLayout.setTopProgress(topProgress);
        mSwipeLayout.setBottomProgress(bottomProgress);
        if (topProgress instanceof SwipeProgressBar) {
            ((SwipeProgressBar) topProgress).setColor(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        }
        if (bottomProgress instanceof SwipeProgressBar) {
            ((SwipeProgressBar) bottomProgress).setColor(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        }
        mSwipeLayout.setMode(SwipeRefreshLayout.Mode.BOTH);
        mSwipeLayout.setLoadNoFull(false);
    }


    /**
     * @param mListView2
     * @param arrayAdapter
     */
    private void setAdapter(ViewGroup mListView2, ArrayAdapter<String> arrayAdapter) {
        for (int i = 0; i < arrayAdapter.getCount(); i++) {
            mListView2.addView(arrayAdapter.getView(i, null, mListView2));
        }
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    ArrayList<String> values = new ArrayList<String>() {{
        for (int i = 0; i < 12; i++) {
            add("value "+i);
        }
    }};

    @Override
    public void onRefresh() {
        mListAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, values);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setAdapter(mListView,(mListAdapter));
                mSwipeLayout.setRefreshing(false);
            }
        }, 2000);
    }

    @Override
    public void onLoad() {
        values.add("Add " + values.size());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mListAdapter.notifyDataSetChanged();
                mSwipeLayout.setLoading(false);
            }
        }, 1000);
    }
}
