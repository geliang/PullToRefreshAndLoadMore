package com.whos.swiperefreshandload;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.whos.swiperefreshandload.view.CircleProgress;
import com.whos.swiperefreshandload.view.SwipeProgressBar;
import com.whos.swiperefreshandload.view.SwipeProgressInterface;
import com.whos.swiperefreshandload.view.SwipeRefreshLayout;
import com.whos.swiperefreshandload.view.SwipeRefreshLayout.OnLoadListener;
import com.whos.swiperefreshandload.view.SwipeRefreshLayout.OnRefreshListener;


@SuppressLint("ResourceAsColor")
public class MainActivity extends Activity implements OnRefreshListener, OnLoadListener{

    protected ListView mListView;
    private ArrayAdapter<String> mListAdapter;
    SwipeRefreshLayout mSwipeLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = (ListView) findViewById(R.id.list);
        mListAdapter = new ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_1, values);
		mListView.setAdapter(mListAdapter);


        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setOnLoadListener(this);
        SwipeProgressInterface topProgress = new SwipeProgressBar(mSwipeLayout);
//        SwipeProgressInterface bottomProgress = new CircleProgress(mSwipeLayout);
        SwipeProgressInterface bottomProgress = new SwipeProgressBar(mSwipeLayout);
        mSwipeLayout.setTopProgress(topProgress);
        mSwipeLayout.setBottomProgress(bottomProgress);
        if (topProgress instanceof SwipeProgressBar) {
            ((SwipeProgressBar) topProgress).setColor(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        }
        if (bottomProgress instanceof SwipeProgressBar) {
            ((SwipeProgressBar) bottomProgress).setColor(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        }
        mSwipeLayout.setMode(SwipeRefreshLayout.Mode.BOTH);
        mSwipeLayout.setLoadNoFull(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        startActivity(new Intent(this,SocrollActivity.class));
        return false;
        
    };
    ArrayList<String> values = new ArrayList<String>() {{
        for (int i = 0; i < 12; i++) {
            add("value "+i);
        }
    }};

    @Override
    public void onRefresh() {
        mListAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, values);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mListView.setAdapter(mListAdapter);
                mSwipeLayout.setRefreshing(false);
            }
        }, 2000);
    }

    @Override
    public void onLoad() {
        values.add("Add " + values.size());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mListAdapter.notifyDataSetChanged();
                mSwipeLayout.setLoading(false);
            }
        }, 1000);
    }
}
