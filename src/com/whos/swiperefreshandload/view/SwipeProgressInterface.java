package com.whos.swiperefreshandload.view;

import android.graphics.Canvas;


/**
 * @author GeLiang
 * @Create at 2015-4-28 上午11:03:34
 * @Version 1.0
 * <p><strong>Custom SwipeProgress </strong></p>
 */
public interface SwipeProgressInterface {

    /**
     * Update the progress the user has made toward triggering the swipe
     * gesture. and use this value to update the percentage of the trigger that
     * is shown.
     */
    public void setTriggerPercentage(float triggerPercentage) ;

    /**
     * Start showing the progress animation.
     */
    public void start();

    /**
     * Stop showing the progress animation.
     */
    public void stop();

    public void draw(Canvas canvas);

    /**
     * @param i
     * @param j
     * @param width
     * @param height
     */
    public void setBounds(int x, int y, int width, int height);

    

}
