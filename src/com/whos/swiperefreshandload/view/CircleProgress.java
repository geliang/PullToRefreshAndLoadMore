package com.whos.swiperefreshandload.view;

import java.util.Random;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.animation.AnimationUtils;

/**
 * @author Administrator
 * @Create at 2015-4-28 上午11:08:30
 * @Version 1.0
 * <p><strong>Features draft description.主要功能介绍</strong></p>
 */
public class CircleProgress implements SwipeProgressInterface {

    // ===========================================================
    // Constants
    // ===========================================================
    
    // Colors used when rendering the animation,
    private View mParent;

    private Rect mBounds ;
    private final Paint mPaint ;

    private float mTriggerPercentage;


    private boolean isAnimationRunning;

    public CircleProgress(View parent) {
        mParent = parent;
        mBounds = new Rect();
        mPaint = new Paint();
        mPaint.setColor(0xffff0000);
    }

    /* 
     * @see com.whos.swiperefreshandload.view.SwipeProgressInterface#setTriggerPercentage(float)
     */
    @Override
    public void setTriggerPercentage(float triggerPercentage) {
        System.out.println("triggerPercentage:"+triggerPercentage+"%");
        mTriggerPercentage = triggerPercentage;

    }

    /* 
     * @see com.whos.swiperefreshandload.view.SwipeProgressInterface#start()
     */
    @Override
    public void start() {
        if (!isAnimationRunning) {
            isAnimationRunning = true;
            ViewCompat.postInvalidateOnAnimation(mParent);
            System.out.println("show progress animation");
        }
    }


    /* 
     * @see com.whos.swiperefreshandload.view.SwipeProgressInterface#stop()
     */
    @Override
    public void stop() {
        if (isAnimationRunning) {
            mTriggerPercentage = 0;
            isAnimationRunning = false;
            ViewCompat.postInvalidateOnAnimation(mParent);
            System.out.println("hide progress ");
        }
    

    }
    Random random = new Random();
    /* 
     * @see com.whos.swiperefreshandload.view.SwipeProgressInterface#draw(android.graphics.Canvas)
     */
    @Override
    public void draw(Canvas canvas) {
        final int width = mBounds.width();
        final int height = mBounds.height();
        final int cx = width / 2;
        final int cy = height / 2;
        final float radius = height*mTriggerPercentage/2;
        canvas.save();
        canvas.translate(mBounds.left, mBounds.top);
        if (isAnimationRunning) {
            System.out.println("draw on  radio "+radius);
            int radios = (int) (height-random.nextInt((int)radius+1));
            System.out.println("draw on random radio "+(radios));
            mPaint.setColor(0xffff0000);
            canvas.drawCircle(cx, height/2, radius, mPaint);
            ViewCompat.postInvalidateOnAnimation(mParent);
        }else{
            System.out.println("draw on touch");
            mPaint.setColor(0xff00ff00);
            canvas.drawCircle(cx, height-radius, radius, mPaint);
        }
        canvas.restore();
    }

    /* 
     * @see com.whos.swiperefreshandload.view.SwipeProgressInterface#setBounds(int, int, int, int)
     */
    @Override
    public void setBounds(int left, int top, int right, int bottom) {
        mBounds.left = left;
        mBounds.top = top;
        mBounds.right = right;
        mBounds.bottom = bottom;
    }

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================

}
